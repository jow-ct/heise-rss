import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:xml/xml.dart' as xml;
import 'webview.dart';

/// Zeigt den Heise-Feed in einem [GridView]
class RSSGrid extends StatefulWidget {
  final String rss;
  final WidgetBuilder drawer;
  RSSGrid(this.rss, this.drawer);
  @override
  State<StatefulWidget> createState() => _RSSGridState();
}

const paddingIntern = 2.0;
const paddingExtern = 4.0;
const minSummarySize = 20;
const maxImgSize = 0.6;

class _GridLayouter extends MultiChildLayoutDelegate {
  @override
  void performLayout(Size size) {
    var sizeTitle = layoutChild(1, BoxConstraints.loose(size));
    positionChild(1, Offset.zero);
    var maxLeft = min(size.width*maxImgSize, size.width-minSummarySize);
    var sizeDate = layoutChild(3, BoxConstraints(maxWidth: maxLeft));
    var sizeImg = layoutChild(4, BoxConstraints(
        maxWidth: maxLeft,
        maxHeight: size.height-sizeDate.height-sizeTitle.height - 2*paddingIntern));
    positionChild(4, Offset(0.0, sizeTitle.height + paddingIntern));
    var sizeSummary = layoutChild(2,
        BoxConstraints(
            maxWidth: size.width-sizeImg.width - paddingIntern,
            maxHeight: size.height-sizeTitle.height - paddingIntern));
    positionChild(2, Offset(sizeImg.width + paddingIntern, sizeTitle.height + paddingIntern));
    positionChild(3, Offset(0.0, sizeTitle.height + sizeImg.height + 2*paddingIntern));
  }

  @override
  bool shouldRelayout(_GridLayouter oldDelegate) => false;
}


class _RSSGridState extends State<RSSGrid> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String _title;
  List<xml.XmlElement> _entries;
  bool _isLoading = false;
  static final DateFormat formatDateTime = DateFormat.MMMMd().add_jm();


  @override
  void initState() {
    super.initState();
    _reload();
  }

  Future<Null> _reload() async {
    if (_isLoading) return;
    setState(() {_isLoading = true;});
    try {
//      await Future.delayed(Duration(seconds:3), (){}); // Verzögerung für Debug-Zwecke
      var feed = xml.parse((await http.get(widget.rss)).body).findElements("feed").first;
      setState(() {
        _title = feed.findElements("title").first.text;
        _entries = feed.findElements("entry").toList();
        _isLoading = false;
      });
    } catch (e) {
      setState(() {_isLoading = false;});
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(e.toString())));
    }
  }

  Widget test1() {
    return Scrollbar(
        child: GridView.builder(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 450,
              mainAxisSpacing: paddingExtern,
              crossAxisSpacing: paddingExtern,
              childAspectRatio: 2,
            ),
            padding: EdgeInsets.all(paddingIntern),
            itemCount: _entries.length,
            itemBuilder: (context, index) {

              var w = _entries[index].findAllElements("img");
              if (w.isEmpty)
                w = xml.parse("<p>${_entries[index].findElements("content")?.first?.text}</p>").findAllElements("img");

              var theme = Theme.of(context).textTheme;
              var title = Text(_entries[index].findElements("title").single.text,
                style: theme.title.apply(fontWeightDelta: 5),
                maxLines: 2, overflow: TextOverflow.ellipsis,);
              var summary = Text(_entries[index].findElements("summary").single.text,
                style: theme.body1,
                maxLines: 100,
                softWrap: true,
                overflow: TextOverflow.ellipsis,);
              var date = Text(
                  formatDateTime.format(DateTime.parse(_entries[index].findElements("published").single.text).toLocal()),
                  style: theme.body1.copyWith(fontStyle: FontStyle.italic));
              var img = w.isEmpty ? FlutterLogo(size:150)
                  : Image.network(w.first.getAttribute("src"));

              return GestureDetector(
                onTap: () => showWebview(context,
                  url: _entries[index].findElements("id").single.text,
                  title: _entries[index].findElements("title").single.text,
                ),
                child: CustomMultiChildLayout(
                    delegate: _GridLayouter(),
                    children: [
                      LayoutId(id: 1, child: title),
                      LayoutId(id: 2, child:
                            ClipRect(child: summary, clipBehavior: Clip.antiAlias,),
                      ),
                      LayoutId(id: 3, child: ClipRect(child: date)),
                      LayoutId(id: 4, child: img),
                    ]
                ),
              );
            } // itemBuilder
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    print("GridView.build isLoading:$_isLoading; items:${_entries?.length ?? "leer"}");
    return Scaffold(
      appBar: AppBar(title: Text(_title ?? widget.rss)),
      drawer: widget.drawer(context),
      key: _scaffoldKey,
      body: _entries == null

        // body: keine Daten
        ? Center(child: _isLoading
          ? CircularProgressIndicator()
          : RaisedButton(
            child: Text("neu laden"),
            onPressed: () => _reload(),
          )
        )

        // body: Daten vorhanden
        : RefreshIndicator(
          onRefresh: () => _reload(),
          child: test1()
        )
    );
  }
}
