import 'dart:async';
import 'package:flutter/material.dart';
import 'package:xml/xml.dart';
import 'package:http/http.dart' as http;
import 'webview.dart';

/// RSS-Reader in einer [material.ListView], siehe auch [c't 18/2018, S. 142]
/// (https://www.heise.de/ct/ausgabe/2018-18-Apps-fuer-Android-und-iOS-entwickeln-mit-Flutter-4132910.html)
class RSSList extends StatefulWidget {
  final String rss;
  final WidgetBuilder drawer;
  RSSList(this.rss, this.drawer);
  @override
  State<StatefulWidget> createState() => _RSSListState();
}

class _RSSListState extends State<RSSList> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String title = "ListView-Reader";

  Future<List<XmlElement>> _reload() async {
    await Future.delayed(Duration(seconds:3), (){}); // Verzögerung für Debug-Zwecke
    var rss = parse((await http.get(widget.rss)).body).findElements("feed").first;
    title = rss.findElements("title").first.text;
    return rss.findElements("entry").toList();
  }

  @override
  Widget build(BuildContext context) => FutureBuilder<List<XmlElement>>(
    future: _reload(),
    builder: (context, result) {

      // lokale Funktion: die Liste der RSS-Einträge
      Widget rssList() => ListView.builder(
        itemCount: result.data.length,
        itemBuilder: (context, index) => ListTile(
          // RSS-Item in ListTile zusammenstellen
          title: Text(result.data[index].findElements("title").single.text),
          subtitle: Text(result.data[index].findElements("summary").single.text),
          leading: Image.network(
            parse("<p>${result.data[index].findAllElements("content").first.text}</p>").
              findAllElements("img").first.getAttribute("src"),
            width: 100.0),
          // RSS-Item startet Browser
          onTap: () => showWebview(context,
            url: result.data[index].findElements("id").single.text,
            title: title,
          )
        )
      );

      print("ListView builder: ${result.connectionState}");
      return Scaffold(
        key: _scaffoldKey, // für showSnackBar wichtig

        appBar: AppBar(
          title: Text(title),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.refresh),
              onPressed: result.connectionState == ConnectionState.done
                  ? () {
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text("Lese $title neu ein...")));
                    setState((){});
                  } : null,
            )
          ],
        ),
        drawer: widget.drawer(context),
        body: result.hasData
          ? result.connectionState == ConnectionState.waiting
            ? Stack(children: <Widget>[
              LinearProgressIndicator(),
              rssList()
            ],)
            : rssList()
          : result.hasError
            ? Text(result.error.toString())
            : Center(child: CircularProgressIndicator())
      );
    } // builder
  );

}
