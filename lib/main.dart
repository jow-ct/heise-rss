import 'dart:io';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'webview.dart';
import 'listview.dart';
import 'gridview.dart';
import 'staggeredgrid.dart';


/*
* Heise RSS-Reader als Flutter-Projekt

TODO heise_rss2 steht noch irgendwo im Android-Teil...
TODO Suche
TODO CupertinoDesign
TODO https://stackoverflow.com/questions/50435196/text-with-inline-images-in-flutter
TODO heise+

* Stand ab Sep 2019
DONE webview_flutter meldet Fehler bei http statt https

* Stand c't 4/2019
* Änderungen gegenüber c't 20/2018, für Folge 3:
DONE WebView von 1.0
DONE StaggeredGridView
DONE GridView: Boxen abgeschnitten
DONE CDATA-Änderung
DONE Drawer
DONE Aufteilung in drei Dateien
DONE Platform.localeName geht bei iOS(-Emulator) nicht

*/

/// Die anzuzeigende URL
const url = "https://www.heise.de/newsticker/heise-atom.xml";

//const url = 'https://www.heise.de/rss/heiseplus.rdf';

void main() async {
  Intl.defaultLocale = Platform.localeName ?? "de_DE";
  await initializeDateFormatting();
  runApp(MaterialApp(
    title: "RSS-Reader", //für Taskliste
    theme: ThemeData(primarySwatch: Colors.lightBlue),
    home: RSSMasonry(url, _mainDrawer),
  ));
}


Drawer _mainDrawer(BuildContext context) {
  print("_mainDrawer");
  return Drawer(
    child: ListView(
      children: [
        DrawerHeader(
          child: Text("RSS-Reader für Heise\nc't Projekt (jow@ct.de)"),
          decoration: BoxDecoration(color: Colors.lightBlueAccent),
        ),
        ListTile(
          title: Text("Heise-Reader per StaggeredGridView\n(c't 4/2019)"),
          leading: Icon(Icons.view_quilt),
          onTap: () => Navigator.push(context, MaterialPageRoute(
            builder: (BuildContext context) => RSSMasonry(url, _mainDrawer)))),
        ListTile(
          title: Text("Heise-Reader per GridView\n(c't 20/2018)"),
          leading: Icon(Icons.view_comfy),
          onTap: () => Navigator.push(context, MaterialPageRoute(
            builder: (BuildContext context) => RSSGrid(url, _mainDrawer)))),
        ListTile(
          title: Text("Heise-Reader per ListView\n(c't 18/2018)"),
          leading: Icon(Icons.view_list),
          onTap: () => Navigator.push(context, MaterialPageRoute(
            builder: (BuildContext context) => RSSList(url, _mainDrawer)))),
        Divider(),
        ListTile(
          title: Text("Heise im Browser"),
          leading: Icon(Icons.public),
          onTap: () => showWebview(context,
              url: "https://www.heise.de", title: "Heise", drawer: _mainDrawer)),
        Divider(),
        AboutListTile(icon: Icon(Icons.android))
      ]
    )
  );
}
