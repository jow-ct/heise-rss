import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:xml/xml.dart' as xml;
import 'webview.dart';

class RSSMasonry extends StatefulWidget {
  final String rss;
  final WidgetBuilder drawer;
  RSSMasonry(this.rss, this.drawer);
  @override
  State<StatefulWidget> createState() => _RSSMasonryState();
}

class _RSSMasonryState extends State<RSSMasonry> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String _title;
  List<xml.XmlElement> _entries;
  bool _isLoading = false;
  static final DateFormat formatDateTime = DateFormat.MMMMd().add_jm();


  @override
  void initState() {
    super.initState();
    _reload();
  }

  Future<Null> _reload() async {
    if (_isLoading) return;
    setState(() {_isLoading = true;});
    try {
//      await Future.delayed(Duration(seconds:3), (){}); // Verzögerung für Debug-Zwecke
      var feed = xml.parse((await http.get(widget.rss)).body).findElements("feed").first;
      setState(() {
        _title = feed.findElements("title").first.text;
        _entries = feed.findElements("entry").toList();
        _isLoading = false;
      });
    } catch (e) {
      setState(() {_isLoading = false;});
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(e.toString())));
    }
  }


  Widget test2() {
    return StaggeredGridView.countBuilder(
      crossAxisCount: (MediaQuery.of(context).size.width / 450).ceil(),
      mainAxisSpacing: 20,
      crossAxisSpacing: 12,
      padding: EdgeInsets.all(4),

      itemCount: _entries.length,
      itemBuilder: (context, index) {
//        print("StaggeredGridView.itemBuilder $index");
        var theme = Theme.of(context).textTheme;
        var title = Text(_entries[index].findElements("title").single.text,
          style: theme.title.apply(fontWeightDelta: 5),
          maxLines: 5, overflow: TextOverflow.ellipsis,);
        var summary = Text(_entries[index].findElements("summary").single.text,
          style: theme.body1, softWrap: true, maxLines: 20, overflow: TextOverflow.ellipsis,);
        var date = Text(formatDateTime.format(DateTime.parse(
            _entries[index].findElements("published").single.text).toLocal()),
          style: theme.body1.copyWith(fontStyle: FontStyle.italic));
        var img = Image.network(xml.parse(
            "<p>${_entries[index].findAllElements("content").first.text}</p>")
          .findAllElements("img").first.getAttribute("src"));
        return GestureDetector(
          onTap: () => showWebview(context,
            url: _entries[index].findElements("id").single.text,
            title: _entries[index].findElements("title").single.text,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              title,
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(flex: 3, child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(child: img, padding: EdgeInsets.fromLTRB(0, 2, 8, 2),),
                      date
                    ]
                  )),
                  Expanded(flex: 2, child: summary)
                ]
              )
            ]
          )
        );
      },
//      staggeredTileBuilder: (index) => StaggeredTile.extent(1, 400)
      staggeredTileBuilder: (index) => StaggeredTile.fit(1)
//      staggeredTileBuilder: (index) => StaggeredTile.count(index % 3 == 0 ? 2 : 1, 1)
    );
  }

  @override
  Widget build(BuildContext context) {
    print("StaggeredGridView.build isLoading:$_isLoading; items:${_entries?.length ?? "leer"}");
    return Scaffold(
      appBar: AppBar(title: Text(_title ?? widget.rss)),
      drawer: widget.drawer(context),
      key: _scaffoldKey,
      body: _entries == null

      // body: keine Daten
        ? Center(child: _isLoading
          ? CircularProgressIndicator()
          : RaisedButton(
            child: Text("neu laden"),
            onPressed: () => _reload(),
          )
        )

      // body: Daten vorhanden
        : RefreshIndicator(
          onRefresh: () => _reload(),
          child: Scrollbar(child: test2())
        )
    );
  }
}
