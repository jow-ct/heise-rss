import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

/// showWebview erzeugt einen Scaffold mit einem Webview,
/// sodass alles zum Package hier gekapselt ist.
/// Momentan: https://pub.dartlang.org/packages/webview_flutter
void showWebview(BuildContext context, {String url, String title, WidgetBuilder drawer}) {
  print("webView $url");
  WebViewController controller;
  Navigator.of(context).push(
    MaterialPageRoute(
      builder: (context) => Scaffold(
        appBar: AppBar(
          title: Text(title ?? url),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.refresh), onPressed: () {
              if (controller!=null) {
                controller.currentUrl().then((_) => print("reload $_"));
                controller.reload().then((_) => print("reloaded."));
              }
            }),
          ],
        ),
        drawer: drawer==null ? null : drawer(context),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.home),
          onPressed: () {if (controller!=null) controller.loadUrl(url);},),
        body: WebView(
          initialUrl: url,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (c) {
            controller = c;
            c.currentUrl().then( (_) => print("webView onCreated $_"));
          }
        )
      )
    )
  );
}
